import { createAppContainer } from "react-navigation";
import { createStackNavigator } from "react-navigation-stack";

import LoginScreen from "./src/screens/LoginScreen";
import MenuScreen from "./src/screens/MenuScreen";

const navigator = createStackNavigator(
  {
    Login: LoginScreen,
    Menu: MenuScreen
  },
  {
    initialRouteName: "Login", // John , cambia esto para que veas el otro screen, por ejemplo a Menu
    headerMode: "none"
  }
);

export default createAppContainer(navigator);
