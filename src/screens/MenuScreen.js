import React from "react";
import { Text, View, StyleSheet, Dimensions } from "react-native";
import { Ionicons, MaterialIcons } from "@expo/vector-icons";

import {
  Container,
  Header,
  Content,
  Footer,
  FooterTab,
  Button,
  Icon
} from "native-base";

const MenuScreen = () => {
  return (
    <Container>
      <Content>
        <View style={[styles.menuItem, styles.backgroundWhite]}>
          <Ionicons style={styles.icon} name="ios-infinite" />
          <Text style={styles.text}> VIAJES </Text>
        </View>
        <View style={[styles.menuItem, styles.backgroundBlueOne]}>
          <MaterialIcons style={styles.icon} name="local-gas-station" />
          <Text style={styles.text}> COMBUSTIBLE </Text>
        </View>
        <View style={[styles.menuItem, styles.backgroundBlueTwo]}>
          <Ionicons style={styles.icon} name="ios-stats" />
          <Text style={styles.text}> INFORMES </Text>
        </View>
        <View style={[styles.menuItem, styles.backgroundBlueThree]}>
          <MaterialIcons style={styles.icon} name="unfold-more" />
          <Text style={styles.text}> OTROS </Text>
        </View>
      </Content>
      <Footer>
        <FooterTab style={{ backgroundColor: mainColor }}>
          <Button>
            <Icon name="apps" />
          </Button>
          <Button>
            <Icon name="camera" />
          </Button>
          <Button>
            <Icon name="navigate" />
          </Button>
          <Button>
            <Icon name="person" />
          </Button>
        </FooterTab>
      </Footer>
    </Container>
  );
};

const { height, width } = Dimensions.get("window");
const mainColor = "#2699FB";

const styles = StyleSheet.create({
  menuItem: {
    alignItems: "center",
    flexDirection: "row",
    backgroundColor: "#ddd",
    height: height * 0.2405,
    paddingLeft: 40
  },
  text: {
    fontSize: 20,
    color: mainColor
  },
  icon: {
    fontSize: 80,
    marginRight: 10,
    color: mainColor
  },
  backgroundWhite: { backgroundColor: "white", paddingTop: 40 },
  backgroundBlueOne: { backgroundColor: "#F1F9FF" },
  backgroundBlueTwo: { backgroundColor: "#BCE0FD" },
  backgroundBlueThree: { backgroundColor: "#7FC4FD" }
});

export default MenuScreen;
