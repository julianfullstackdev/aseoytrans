import React, { useState, useEffect } from "react";

import {
  View,
  Text,
  TextInput,
  StyleSheet,
  Dimensions,
  KeyboardAvoidingView
} from "react-native";

import getTheme from "../native-base-theme/components";
import commonColor from "../native-base-theme/variables/commonColor";

import { MaterialCommunityIcons } from "@expo/vector-icons";
// Los iconos son de Expo, esta es la lista completa https://expo.github.io/vector-icons/

import {
  Container,
  Content,
  Form,
  Picker,
  Icon,
  List,
  ListItem,
  Button,
  CheckBox,
  StyleProvider
} from "native-base";

// Estoy usando Native-base para hacer algunos componentes, puedes ver mas aquí  https://docs.nativebase.io/Components.html#Components

/* 
los estilos en RN son en linea pero puede usarse 
un objeto externo para aplicarlos mas fácil y organizada. */

// Ahora es habitual en React declarar componentes funcionales y no basados en clases o Class Components.

const mainColor = "#2699FB";

const LoginScreen = () => {
  const [selected, setSelected] = useState("");
  const [remember, setRemember] = useState(true);

  const onValueChange = value => {
    setSelected(value);
  };

  const onRememberChange = () => {
    setRemember(!remember);
  };

  return (
    <StyleProvider style={getTheme(commonColor)}>
      <Container>
        <View style={styles.header}>
          <Text style={styles.title}>ASEYTRANS APP</Text>
          <MaterialCommunityIcons
            name="map-marker-radius"
            style={styles.iconStyle}
          />
        </View>
        <KeyboardAvoidingView behavior="padding" style={{ flex: 1 }}>
          <Content>
            <Form style={styles.formBox}>
              <Text style={styles.titleInput}>Identificación</Text>
              <TextInput style={styles.input} placeholder="71790862" />

              <Text style={styles.titleInput}>Placa</Text>

              {/* Para dar estilos al select se debe usar un View externo. */}
              <View style={styles.select}>
                <Picker
                  mode="dropdown"
                  iosHeader="Selecciona la placa"
                  iosIcon={<Icon name="arrow-down" />}
                  selectedValue={selected}
                  onValueChange={onValueChange}
                >
                  <Picker.Item label="HJ987" value="key0" />
                  <Picker.Item label="BXS805" value="key1" />
                  <Picker.Item label="ITX0620" value="key2" />
                  <Picker.Item label="JAC897" value="key3" />
                </Picker>
              </View>

              <Text style={styles.titleInput}>Clave</Text>
              <TextInput
                secureTextEntry
                style={styles.input}
                placeholder="*****"
              />

              <List style={{ alignSelf: "center", marginTop: 10 }}>
                <ListItem onPress={onRememberChange}>
                  <CheckBox
                    style={styles.checkbox}
                    checked={remember}
                    onPress={onRememberChange}
                  />
                  <Text
                    style={{ marginLeft: 10, color: mainColor, fontSize: 18 }}
                  >
                    Recordar datos
                  </Text>
                </ListItem>
              </List>
            </Form>

            <Button rounded style={styles.button}>
              <Text class="hola" style={{ color: "white" }}>
                INGRESAR
              </Text>
            </Button>
          </Content>
        </KeyboardAvoidingView>
      </Container>
    </StyleProvider>
  );
};

const { height, width } = Dimensions.get("window");
/* Con esto obtengo las dimensiones del dispositivo, 
RN trabaja con unidades, no puedo asignar porcentajes, 
este es un truco para hacerlo. */

const styles = StyleSheet.create({
  mainContainer: {
    backgroundColor: "white"
  },
  header: {
    backgroundColor: mainColor,
    height: 200,

    justifyContent: "center"
  },
  title: {
    textAlign: "center",
    fontSize: 25,
    color: "white",
    fontWeight: "bold",
    top: 20
  },
  iconStyle: {
    fontSize: 45,
    alignSelf: "center",
    top: 20,
    color: "white"
  },
  formBox: {
    marginHorizontal: width * 0.1 // Esto es el 15% de margen en ambos lados: left y right
  },
  titleInput: {
    color: mainColor,
    fontSize: 18,
    marginTop: 20
  },
  input: {
    borderColor: "#BCE0FD",
    color: mainColor,
    borderWidth: 2, // En RN tienes que dar un ancho al borde, sino no se aplica ni el color ni el radius.
    borderRadius: 50,
    fontSize: 18,
    paddingHorizontal: 20,
    paddingVertical: 10,
    marginTop: 10
  },
  select: {
    borderColor: "#BCE0FD",
    borderWidth: 2,
    color: mainColor,
    borderRadius: 50
  },
  checkbox: {
    backgroundColor: "white",
    color: "#B7D8F0",
    borderRadius: 3,
    height: 30,
    width: 35,
    justifyContent: "center",
    textAlign: "center",
    paddingLeft: 9,
    paddingTop: 10
  },
  button: {
    width: width * 0.7,
    backgroundColor: mainColor,
    alignSelf: "center",
    justifyContent: "center",
    marginTop: 30
  }
});

export default LoginScreen;
